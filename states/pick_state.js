// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
goog.provide('sheepcast.games.spellcast.states.PickState');


//goog.require('sheepcast.games.spellcast.GameConstants');
goog.require('sheepcast.games.spellcast.State');
//goog.require('sheepcast.games.spellcast.messages.DifficultySetting');
goog.require('sheepcast.games.spellcast.messages.GameStateId');



/**
 * @param {!sheepcast.games.spellcast.SpellcastGame} game
 * @param {!sheepcast.games.spellcast.StateMachine} stateMachine
 * @constructor
 * @implements {sheepcast.games.spellcast.State}
 */
sheepcast.games.spellcast.states.PickState = function(game,
    stateMachine, sprites, canvasWidth, canvasHeight, container) {
		
  /** @private {Array} */
  this.sprites_ = sprites;
  this.canvasWidth_ = canvasWidth;
  this.canvasHeight_ = canvasHeight;
  this.container_ = container;
  
  /** @private {!sheepcast.games.spellcast.SpellcastGame} */
  this.game_ = game;

  /** @private {!cast.receiver.games.GameManager} */
  this.gameManager_ = this.game_.gameManager_;

  /** @private {!sheepcast.games.spellcast.StateMachine} */
  this.stateMachine_ = stateMachine;

  /**
   * A reusable array of ready players that is updated in #onUpdate.
   * @private {!Array.<!cast.receiver.games.PlayerInfo>}
   */
  this.readyPlayers_ = [];

  /**
   * The player ID of the host.
   * @private {?string}
   */
  this.hostPlayerId_ = null;

  /**
   * Pre-bound handler when a player becomes ready.
   * @private {function(!cast.receiver.games.Event)}
   */
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);
  this.numberSpritesAdded_ = 0;
};


/** @override */
sheepcast.games.spellcast.states.PickState.prototype.onEnter =
    function(previousStateId) {
	    this.game_.blind = [];
	    this.game_.clearDrawnCards();
  // Add any players that are ready.
  this.readyPlayers_ = this.gameManager_.getPlayersInState(
      cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  for (var i = 0; i < this.readyPlayers_.length; i++) {
    var player = this.readyPlayers_[i];
    this.addPlayer_(player.playerId, player.playerData, i == 0);
  }

  // Listen to when a player is ready or starts playing.
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_READY,
      this.boundPlayerReadyCallback_);
	  
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);

  // The game is showing lobby screen and the lobby is open for new players.
  this.gameManager_.updateGameplayState(
      cast.receiver.games.GameplayState.SHOWING_INFO_SCREEN, null);
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.OPEN,
      null);
  this.currUser = (this.game_.deal_player + 1) % this.game_.players.length; // start pick/pass to the left of the dealer
  console.log("deal_player: " + this.game_.deal_player + "\tcurrUser: " + this.currUser);
  this.letPlayerPickPass(this.currUser);
};


/** @override */
sheepcast.games.spellcast.states.PickState.prototype.onUpdate =
    function() {
  var now = Date.now();
  this.readyPlayers_ = this.gameManager_.getPlayersInState(
      cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  var numberReadyPlayers = this.readyPlayers_.length;

  // Check if we need to update who is the game host.
  var hostPlayer = this.hostPlayerId_ ?
      this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  if (hostPlayer && this.hostPlayerId_ && numberReadyPlayers > 0 &&
      hostPlayer.playerState != cast.receiver.games.PlayerState.READY) {
    this.updateHost_(this.readyPlayers_[0].playerId);
  }

  if (this.game_.randomAiEnabled && now > this.waitingForRandomAiDelay_) {
    if (numberReadyPlayers <
        sheepcast.games.spellcast.GameConstants.MAX_PLAYERS) {
      this.game_.testCreatePlayer();
    } else {
      this.game_.testStartGame();
    }
    this.waitingForRandomAiDelay_ += 1000;
  }
};

sheepcast.games.spellcast.states.PickState.prototype.pickPlayer = function(playerNum) {
    var cards = this.game_.deck.splice(0,2);
	var mm = { type:"blind", cards:cards};
	this.gameManager_.sendGameMessageToPlayer(this.game_.players[playerNum].playerId, mm);
	this.game_.pick_player = playerNum;
	if (this.game_.pick_player == this.game_.partner_player) {
	  this.game_.partner_player = -1;
	  console.log("Pick became own partner");
	}
    this.game_.round_scores = [];
};

sheepcast.games.spellcast.states.PickState.prototype.letPlayerPickPass = function(playerNum) {
	var mm = { type:"let_pick_pass" };
	this.game_.headerText.text = this.game_.players[playerNum].name + "'s turn!\nPick or Pass?"
	this.gameManager_.sendGameMessageToPlayer(this.game_.players[playerNum].playerId, mm);
};

sheepcast.games.spellcast.states.PickState.prototype.onGameMessage_ =
	function(event){if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  var message = /** @type {!sheepcast.games.spritedemo.SpritedemoMessage} */ (
      event.requestExtraMessageData);
  var SpritedemoMessageType = sheepcast.games.spritedemo.SpritedemoMessageType;

	console.log(message);
	if (message.type == SpritedemoMessageType.PICK_PASS) {
		if (message.pick_pass == "PASS") { // player passes
    	    console.log("deal_player: " + this.game_.deal_player + "\t Passed by User: " + this.currUser);

            this.currUser = (this.currUser + 1) % this.game_.players.length; // go to next user
			if (this.currUser == this.game_.deal_player) { // force last user to bury
				console.log("force pick currUser" + this.currUser);
				this.pickPlayer(this.currUser);
				var name = this.game_.players[this.currUser].name;
				this.game_.headerText.text = name + " must pick.\nAll other players have passed."
			} else { // let next user pick/pass
				this.letPlayerPickPass(this.currUser);
			}
		} else { // if player picks
			this.pickPlayer(this.currUser);
		}


	} else {
	    this.game_.blind.push(message.cardString);
	    console.log("Blind length: " + this.game_.blind.length);
		this.game_.round_scores[this.game_.pick_player] += cardPoints(message.cardString);

        // initialize some game variables
	    if (this.game_.blind.length == 2) {
            this.game_.round_count = 1;
            this.game_.trick_count = [];
            for (var p in this.game_.players) {
              this.game_.round_scores[p] = 0;
              this.game_.trick_count[p] = 0;
            }
            this.stateMachine_.goToState(
                sheepcast.games.spellcast.messages.GameStateId.ROUND);
	    }
    }
 };

/**
 * Sets a player to become the host and updates the player data.
 * @param {string} newHostPlayerId
 * @param {Object=} opt_newHostPlayerData
 * @private
 */
sheepcast.games.spellcast.states.PickState.prototype.updateHost_ =
    function(newHostPlayerId, opt_newHostPlayerData) {
  if (newHostPlayerId == this.hostPlayerId_) {
    return;
  }

  if (newHostPlayerId != this.hostPlayerId_ && this.hostPlayerId_ &&
      this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
    var oldHostPlayer = this.gameManager_.getPlayer(this.hostPlayerId_);
    var oldHostPlayerData = oldHostPlayer.playerData || {};
    oldHostPlayerData['host'] = false;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, oldHostPlayerData,
        /* opt_noBroadcastUpdate*/ false);
  }

  var newHostPlayerData = null;
  if (opt_newHostPlayerData) {
    newHostPlayerData = opt_newHostPlayerData;
  } else {
    var newHostPlayer = this.gameManager_.getPlayer(newHostPlayerId);
    newHostPlayerData = newHostPlayer.playerData || {};
  }
  newHostPlayerData['host'] = true;
  this.hostPlayerId_ = newHostPlayerId;
  this.gameManager_.updatePlayerData(this.hostPlayerId_, newHostPlayerData);
};


/** @override */
sheepcast.games.spellcast.states.PickState.prototype.onExit =
    function(nextStateId) {
  // Stop listening to player events in this state.
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_READY,
      this.boundPlayerReadyCallback_);
	  
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);

  // Update all ready players to playing state. Hide ready and playing players.
  var players = this.gameManager_.getPlayers();
  for (var i = 0; i < players.length; i++) {
    var playerState = players[i].playerState;
    if (playerState == cast.receiver.games.PlayerState.READY) {
      this.gameManager_.updatePlayerState(players[i].playerId,
          cast.receiver.games.PlayerState.PLAYING, null);
    }
  }

  // The lobby is now closed for new players.
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED,
      null);
  //this.game_.getLobbyDisplay().deactivate();
};
