// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
goog.provide('sheepcast.games.spellcast.states.RoundState');


//goog.require('sheepcast.games.spellcast.GameConstants');
goog.require('sheepcast.games.spellcast.State');
//goog.require('sheepcast.games.spellcast.messages.DifficultySetting');
goog.require('sheepcast.games.spellcast.messages.GameStateId');



/**
 * @param {!sheepcast.games.spellcast.SpellcastGame} game
 * @param {!sheepcast.games.spellcast.StateMachine} stateMachine
 * @constructor
 * @implements {sheepcast.games.spellcast.State}
 */
sheepcast.games.spellcast.states.RoundState = function(game,
    stateMachine, sprites, canvasWidth, canvasHeight, container) {
		
  /** @private {Array} */
  this.sprites_ = sprites;
  this.canvasWidth_ = canvasWidth;
  this.canvasHeight_ = canvasHeight;
  this.container_ = container;
  
  /** @private {!sheepcast.games.spellcast.SpellcastGame} */
  this.game_ = game;

  /** @private {!cast.receiver.games.GameManager} */
  this.gameManager_ = this.game_.gameManager_;

  /** @private {!sheepcast.games.spellcast.StateMachine} */
  this.stateMachine_ = stateMachine;

  /**
   * A reusable array of ready players that is updated in #onUpdate.
   * @private {!Array.<!cast.receiver.games.PlayerInfo>}
   */
  this.readyPlayers_ = [];

  /**
   * The player ID of the host.
   * @private {?string}
   */
  this.hostPlayerId_ = null;

  /**
   * Pre-bound handler when a player becomes ready.
   * @private {function(!cast.receiver.games.Event)}
   */
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);
};


/** @override */
sheepcast.games.spellcast.states.RoundState.prototype.onEnter = function(previousStateId) {

  // Listen to when a player is ready or starts playing.
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);

  // The game is showing lobby screen and the lobby is open for new players.
  this.gameManager_.updateGameplayState(
      cast.receiver.games.GameplayState.SHOWING_INFO_SCREEN, null);
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.OPEN,
      null);

  this.currTrick = [];
  this.currUser = this.game_.startTrickPlayer;
  console.log("Deal player: " + this.game_.deal_player + "\tStarting player: " + this.currUser + "\tPlayer length: " + this.game_.players.length);
  this.ledCard = "";
  this.RequestCard(this.currUser, this.ledCard);
};

// request player to play card, also send the player the led card of the round if it exists
sheepcast.games.spellcast.states.RoundState.prototype.RequestCard = function(playerNum, ledCard) {
	var mm = { type:"PlayCard", ledCard:ledCard};
	this.gameManager_.sendGameMessageToPlayer(this.game_.players[playerNum].playerId, mm);
	console.log("Player " + playerNum + " can now play a card with ledCard: " + ledCard);

	this.game_.headerText.text = this.game_.players[playerNum].name + "'s turn!\nPlay a card.";
	//this.game_.container_.addchild( this.game_.headerText );

	};

/** @override */
sheepcast.games.spellcast.states.RoundState.prototype.onUpdate =
    function() {
  var now = Date.now();
  this.readyPlayers_ = this.gameManager_.getPlayersInState(
      cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  var numberReadyPlayers = this.readyPlayers_.length;

  // Check if we need to update who is the game host.
  var hostPlayer = this.hostPlayerId_ ?
      this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  if (hostPlayer && this.hostPlayerId_ && numberReadyPlayers > 0 &&
      hostPlayer.playerState != cast.receiver.games.PlayerState.READY) {
    this.updateHost_(this.readyPlayers_[0].playerId);
  }

  if (this.game_.randomAiEnabled && now > this.waitingForRandomAiDelay_) {
    if (numberReadyPlayers <
        sheepcast.games.spellcast.GameConstants.MAX_PLAYERS) {
      this.game_.testCreatePlayer();
    } else {
      this.game_.testStartGame();
    }
    this.waitingForRandomAiDelay_ += 1000;
  }
};

sheepcast.games.spellcast.states.RoundState.prototype.onGameMessage_ =
	function(event){if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  var message = /** @type {!sheepcast.games.spritedemo.SpritedemoMessage} */ (
      event.requestExtraMessageData);
  var SpritedemoMessageType = sheepcast.games.spritedemo.SpritedemoMessageType;
  console.log(message);

  // A player played a card
  if (message.type == SpritedemoMessageType.CARD) {
    var card = message.cardString;

    if (this.ledCard == "") {
      this.game_.clearDrawnCards();
    }

    // display card
    var cardFilepath = 'assets/';
    console.log('DisplayCard: playedUser '+this.currUser);
    var cardFilepath = cardFilepath.concat(message.cardFilename);
    var sprite = PIXI.Sprite.fromImage( cardFilepath );
    sprite.anchor.x = 0.5;
    sprite.anchor.y = 0.5;
    sprite.scale.x = sprite.scale.y = sheepcast.games.spritedemo.SpritedemoGame.SCALE * 2;

	// Arrange cards in a semi-circle based on the player number.
	switch(this.currUser){
		case 0:
			sprite.position.x = this.canvasWidth_ * 1 / 6;
			sprite.position.y = this.canvasHeight_ * 0.40;
			break;
		case 1:
			sprite.position.x = this.canvasWidth_ * 2 / 6;
			sprite.position.y = this.canvasHeight_ * 0.58;
			break;
		case 2:
			sprite.position.x = this.canvasWidth_ * 3 / 6;
			sprite.position.y = this.canvasHeight_ * 0.67;
			break;
		case 3:
			sprite.position.x = this.canvasWidth_ * 4 / 6;
			sprite.position.y = this.canvasHeight_ * 0.58;
			break;
		case 4:
			sprite.position.x = this.canvasWidth_ * 5 / 6;
			sprite.position.y = this.canvasHeight_ * 0.40;
			break;
	}
	
	
    this.game_.container_.addChild(sprite);
	
	
	
    this.currUser = (this.currUser + 1) % this.game_.players.length;
    if (this.ledCard == "") {
      this.ledCard = card;
      console.log("ledCard set: " + card)
    }

    this.currTrick.push(card);
    console.log(this.currTrick);

    if (this.currTrick.length == this.game_.players.length) {
      console.log("End of a round: " + this.game_.round_count);

      // Broadcast the trick
      var msg = { type:"trick", cards: this.currTrick};
      console.log("Broadcast msg: " + msg);
      this.gameManager_.sendGameMessageToAllConnectedPlayers(msg);

      //console.log("TODO: determine winner");
      var winner = this.getWinner();
      this.game_.startTrickPlayer = winner;
      this.game_.trick_count[winner] += 1;

      // save points
      var points = this.calculatePoints();
      this.game_.round_scores[winner] += points;
      console.log("Winner: player " + winner + "\tpoints: " + points);
	  

      this.game_.round_count++;
      if (this.game_.round_count <= 6) {
         this.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.ROUND);
		 var winnerName = this.game_.players[winner].name;
		 this.game_.headerText.text = winnerName + " takes the trick!\n" 
					+ winnerName + ", play the lead card for the next trick.";
      } 

      else {
        console.log("TODO: SCORE STATE");
        console.log("Round Scores: " + this.game_.round_scores);
		
		this.assignPoints();


        this.game_.deal_player = (this.game_.deal_player + 1) % this.game_.players.length;
        this.game_.stateMachine_.goToState(
            sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS);
      }
    }
    else{
      this.RequestCard(this.currUser, this.ledCard);
    }
  }
 };

 sheepcast.games.spellcast.states.RoundState.prototype.calculatePoints = function() {
  var sum = 0;
  for (var card in this.currTrick) {
    sum += cardPoints(this.currTrick[card]);
  }
  return sum;
 }
 sheepcast.games.spellcast.states.RoundState.prototype.assignPoints = 
 function(){
        var PickTeamPoints = this.game_.round_scores[this.game_.pick_player];
        var PickTeamTricks = this.game_.trick_count[this.game_.pick_player];
        if (this.game_.partner_player != -1) {
          PickTeamPoints += this.game_.round_scores[this.game_.partner_player];
          PickTeamTricks += this.game_.trick_count[this.game_.partner_player];
        }
		
		this.game_.virgin = false;
		
		var PickerAloneScore;
		var PickerScore;
		var PartnerScore;
		var OpponentScore;

        var pick_won = true;
        if (PickTeamTricks == 6) {
          // perfect, opponents lose 3
          console.log("Pick Team wins took all tricks");
		  this.game_.headerText.text = "Picker Team wins!\nPerfect hand!";
		  PickerAloneScore = 12;
		  PickerScore = 6;
		  PartnerScore = 3;
		  OpponentScore = -3;
        } 
		else if (PickTeamTricks == 0) {
          // terrible, opponents gain 3
          console.log("Opponent Team wins took all tricks");
		  this.game_.headerText.text = "Opponent Team wins!\nPerfect hand!";
		  PickerAloneScore = -12;
		  PickerScore = -6;
		  PartnerScore = -3;
		  OpponentScore = 3;
          pick_won = false;
        } 
		else if (PickTeamPoints >= 91) {
          // good, opponents lose 2
          console.log("Pick Team wins greatly");
		  this.game_.headerText.text = "Picker Team wins!\nThe Opponents did not get Schneidered!";
		  PickerAloneScore = 8;
		  PickerScore = 4;
		  PartnerScore = 2;
		  OpponentScore = -2;
        } 
		else if (PickTeamPoints >= 61) {
          // ok, opponents lose 1
          console.log("Pick Team wins normally");
		  this.game_.headerText.text = "Picker Team wins!";
		  PickerAloneScore = 4;
		  PickerScore = 2;
		  PartnerScore = 1;
		  OpponentScore = -1;
        } 
		else if (PickTeamPoints >= 31) {
          // bad, opponents gain 1
          console.log("Opponent Team wins normally");
		  this.game_.headerText.text = "Opponent Team wins!";
          pick_won = false;
		  PickerAloneScore = -4;
		  PickerScore = -2;
		  PartnerScore = -1;
		  OpponentScore = 1;
        } 
		else {
          // really bad, opponents gain 2
          console.log("Opponent Team wins greatly");
		  this.game_.headerText.text = "Opponent Team wins!\nThe Picker did not get Schneidered!";
		  PickerAloneScore = -8;
		  PickerScore = -4;
		  PartnerScore = -2;
		  OpponentScore = 2;
          pick_won = false;
        }
		
		for(var i = 0; i < this.game_.players.length; i++){
			
			var playerID = this.game_.players[i].playerId;
			
			var scoreModify;
			var scoreModifyText;
			
			
			if ( i == this.game_.partner_player)
				scoreModify = PartnerScore;
			
			else if (i == this.game_.pick_player){
				if(this.game_.partner_player == -1)
					scoreModify = PickerAloneScore;
				else
					scoreModify = PickerScore;
			}
			
			else
				scoreModify = OpponentScore;
			
				
			this.game_.playerScores_[playerID] += scoreModify;
			if(scoreModify > 0){
				scoreModifyText = new PIXI.Text( '+' + scoreModify,
						{font : '24px Arial', align : 'center', fill: "#00ff00"});
			}
			else{
				scoreModifyText = new PIXI.Text( scoreModify,
						{font : '24px Arial', align : 'center', fill: "#ff0000"});
			}
			 
			scoreModifyText.position.x = this.game_.canvasWidth_ * (i+1) / 6;
			scoreModifyText.position.y = this.game_.playerTexts[i].position.y
										+ ( this.game_.canvasHeight_ * 0.07 );
			scoreModifyText.anchor.x = 0.5;
			scoreModifyText.anchor.y = 0.5;
			
			this.game_.container_.addChild(scoreModifyText);
		}
		
		// Display updated scores.
		
		this.game_.updatePlayerTexts();

        // Send game message for stat
        for (var player in this.game_.players){
          var pick = (this.game_.pick_player == player) || (this.game_.partner_player == player) ? true : false;
          var count = this.game_.trick_count[player];
          var msg = { type: "end", won: pick_won, pick: pick, count: count};
          console.log("End message to player " + player + ": " + msg);
          this.gameManager_.sendGameMessageToPlayer(this.game_.players[player].playerId, msg);
        }
	 
 }

 function cardPoints(card) {
  var rank = card.split(" ")[0];
  switch (rank) {
    case "Ace": return 11;
    case "King": return 4;
    case "Queen": return 3;
    case "Jack": return 2;
    case "10": return 10;
    default: return 0;
  }
 }

function isTrump(card) {
  var card_split = card.split(" ")
  if (card_split[2] == "Diamonds") {
    return true;
  }
  switch (card_split[0]) {
    case "Queen": return true;
    case "Jack": return true;
  }
  return false;
}

sheepcast.games.spellcast.states.RoundState.prototype.getWinner = function() {
     var cards = this.currTrick;
     console.log("getWinner is checking trick: " + cards);
     var parsing = cards[0].split(" ");//in javascript use space instead of " "
     var winnerIndex = 0;
     var winningSuit = parsing[2];
     var winningValue = getValue(parsing[0]);
     var tempValue;
     var tempSuit = cards[2];

     var suitLed = parsing[2];
     if (isTrump(cards[0])) {
        winningSuit = "Trump";
        suitLed = "Trump";
     }

     for (var i = 0; i < cards.length; i++){
         //there is a split funciton in javascript
         var card = cards[i];
         parsing = card.split(" ");//in javascript use space instead of " "
         tempValue = getValue(parsing[0]);//gets the value of the card
         tempSuit = parsing[2];//gets the suit of the card
         console.log("winning card value:" + winningValue);
         console.log("temp card value:" + tempValue);

         //special cases for trump
         if (isTrump(card)){
             //for a Jack
             if (tempValue == 12){

                 if (tempSuit == "Hearts") {
                     tempValue += 1;
                 } else if (tempSuit == "Spades") {
                     tempValue += 2;
                 } else if (tempSuit == "Clubs") {
                     tempValue += 3;
                 }

                 //for a Queen
             } else if (tempValue == 13){
                 tempValue = tempValue + 10;
                 if (tempSuit == "Hearts") {
                     tempValue += 1;
                 } else if (tempSuit == "Spades") {
                     tempValue += 2;
                 } else if (tempSuit == "Clubs") {
                     tempValue += 3;
                 }
             }
             //it is trump suit
             tempSuit = "Trump";
         }

         //if the current card is trump
         if (tempSuit == "Trump") {

             //if the currently winning card is trump
             if (winningSuit == "Trump"){
                 //if the current card is better than the winning card
                 if (winningValue < tempValue){
                     winningSuit = tempSuit;
                     winnerIndex = i;
                     winningValue = tempValue;
                 }

                 //if the winning card is not trump
             } else {
                 winningSuit = tempSuit;
                 winnerIndex = i;
                 winningValue = tempValue;
             }

             //if the current card is not trump
         } else {

             //if the winning card is trump
             if (winningSuit == "Trump") {
                 //do nothing

                 //if the winning card is not trump
             } else {

                 //if the current card is the led suit and not trump
                 if (tempSuit == suitLed) {

                     //if the winning card is the led suit and not trump
                     if (winningSuit == suitLed) {
                         if (tempValue > winningValue){
                             winningSuit = tempSuit;
                             winnerIndex = i;
                             winningValue = tempValue;
                         }

                         //if the winning card is not the led suit or trump
                     } else {
                         winningSuit = tempSuit;
                         winnerIndex = i;
                         winningValue = tempValue;
                     }
                 }
             }
         }

     }
  console.log("winner Index: " + winnerIndex + "\tStartTrickPlayer: " + this.game_.startTrickPlayer + "\tplayerLength: " + this.game_.players.length);
 
  return (this.game_.startTrickPlayer + winnerIndex) % this.game_.players.length;
};


function getValue (stringRep ){
    if (stringRep == "Ace") {
        return 11;
    } else if (stringRep == "Jack") {
        return 12;
    } else if (stringRep == "Queen") {
        return 13;
    } else if (stringRep == "King") {
        return 9;
    } else if (stringRep == "10") {
        return 10;
    } else if (stringRep == "9") {
        return 8;
    } else if (stringRep == "8") {
        return 7;
    } else if (stringRep == "7") {
        return 6;
    } else {
        return 0;
    }
}
/**
 * Sets a player to become the host and updates the player data.
 * @param {string} newHostPlayerId
 * @param {Object=} opt_newHostPlayerData
 * @private
 */
sheepcast.games.spellcast.states.RoundState.prototype.updateHost_ =
    function(newHostPlayerId, opt_newHostPlayerData) {
  if (newHostPlayerId == this.hostPlayerId_) {
    return;
  }

  if (newHostPlayerId != this.hostPlayerId_ && this.hostPlayerId_ &&
      this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
    var oldHostPlayer = this.gameManager_.getPlayer(this.hostPlayerId_);
    var oldHostPlayerData = oldHostPlayer.playerData || {};
    oldHostPlayerData['host'] = false;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, oldHostPlayerData,
        /* opt_noBroadcastUpdate*/ false);
  }

  var newHostPlayerData = null;
  if (opt_newHostPlayerData) {
    newHostPlayerData = opt_newHostPlayerData;
  } else {
    var newHostPlayer = this.gameManager_.getPlayer(newHostPlayerId);
    newHostPlayerData = newHostPlayer.playerData || {};
  }
  newHostPlayerData['host'] = true;
  this.hostPlayerId_ = newHostPlayerId;
  this.gameManager_.updatePlayerData(this.hostPlayerId_, newHostPlayerData);
};


/** @override */
sheepcast.games.spellcast.states.RoundState.prototype.onExit =
    function(nextStateId) {
  // Stop listening to player events in this state.
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_READY,
      this.boundPlayerReadyCallback_);
	  
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);

  // Update all ready players to playing state. Hide ready and playing players.
  var players = this.gameManager_.getPlayers();
  for (var i = 0; i < players.length; i++) {
    if (players[i].playerState == cast.receiver.games.PlayerState.READY) {
      this.gameManager_.updatePlayerState(players[i].playerId,
          cast.receiver.games.PlayerState.PLAYING, null);
    }
  }

  // The lobby is now closed for new players.
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED,
      null);
  //this.game_.getLobbyDisplay().deactivate();
};
