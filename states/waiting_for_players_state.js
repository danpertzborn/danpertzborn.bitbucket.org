// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
goog.provide('sheepcast.games.spellcast.states.WaitingForPlayersState');


//goog.require('sheepcast.games.spellcast.GameConstants');
goog.require('sheepcast.games.spellcast.State');
//goog.require('sheepcast.games.spellcast.messages.DifficultySetting');
goog.require('sheepcast.games.spellcast.messages.GameStateId');



/**
 * @param {!sheepcast.games.spellcast.SpellcastGame} game
 * @param {!sheepcast.games.spellcast.StateMachine} stateMachine
 * @constructor
 * @implements {sheepcast.games.spellcast.State}
 */
sheepcast.games.spellcast.states.WaitingForPlayersState = function(game,
    stateMachine) {
  /** @private {!sheepcast.games.spellcast.SpellcastGame} */
  this.game_ = game;

  /** @private {!cast.receiver.games.GameManager} */
  this.gameManager_ = this.game_.gameManager_;

  /** @private {!sheepcast.games.spellcast.StateMachine} */
  this.stateMachine_ = stateMachine;

  /**
   * A reusable array of ready players that is updated in #onUpdate.
   * @private {!Array.<!cast.receiver.games.PlayerInfo>}
   */
  this.game_.players = [];

  /**
   * The player ID of the host.
   * @private {?string}
   */
  this.hostPlayerId_ = null;

  /**
   * Pre-bound handler when a player becomes ready.
   * @private {function(!cast.receiver.games.Event)}
   */
  this.boundPlayerReadyCallback_ = this.onPlayerReady_.bind(this);
  this.playersAvailable = 0;
};


/** @override */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onEnter =
    function(previousStateId) {
  // Add any players that are ready.
  
  
  
  this.game_.players = this.gameManager_.getPlayersInState(
      cast.receiver.games.PlayerState.READY, this.game_.players);
  for (var i = 0; i < this.game_.players.length; i++) {
    this.addPlayer_(this.game_.players[i].playerId, 
					this.game_.players[i].playerData, 
					i == 0);
  }

  // Listen to when a player is ready or starts playing.
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_READY,
      this.boundPlayerReadyCallback_);

	
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_AVAILABLE,
      this.onPlayerAvailable_.bind(this));
	  
  this.onPlayerAvailable_();
	  
};

sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onPlayerAvailable_
	= function(){
	console.log("onPlayerAvailable_()");
	var players = this.gameManager_.getConnectedPlayers();
	
	for (i in players){
		
		if( this.game_.virgin )
			this.game_.playerTexts[i].text = "Player " +  (parseInt(i)+1) + " is connected."
		
		if( typeof (this.game_.playerScores_[players[i].playerId]) == "undefined"){
			console.log("i = " + i 
				+ ",    typeof i = " + (typeof i)
				+ ",    playerId = " + players[i].playerId);
			this.game_.playerScores_[players[i].playerId] = 0;
		}
	}
	  
}

/** @override */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onUpdate =
    function() {
  var now = Date.now();
  this.game_.players = this.gameManager_.getPlayersInState(
      cast.receiver.games.PlayerState.READY, this.game_.players);
  var numberReadyPlayers = this.game_.players.length;

  // Check if we need to update who is the game host.
  var hostPlayer = this.hostPlayerId_ ?
      this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  if (hostPlayer && this.hostPlayerId_ && numberReadyPlayers > 0 &&
      hostPlayer.playerState != cast.receiver.games.PlayerState.READY) {
    this.updateHost_(this.game_.players[0].playerId);
  }

  if (this.game_.randomAiEnabled && now > this.waitingForRandomAiDelay_) {
    if (numberReadyPlayers <
        sheepcast.games.spellcast.GameConstants.MAX_PLAYERS) {
      this.game_.testCreatePlayer();
    } else {
      this.game_.testStartGame();
    }
    this.waitingForRandomAiDelay_ += 1000;
  }
};


/**
 * Handles when a player becomes ready.
 * @param {!cast.receiver.games.Event} event
 * @private
 */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onPlayerReady_ =
    function(event) {
	event.playerInfo.name = event.requestExtraMessageData.name;
	console.log('waiting for players state, onPlayerReady called');
  if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  console.log("**** PLAYER READY UP ****")
  if (this.game_.players.length >
      this.game_.MAX_PLAYERS_) {
    return;
  }

  this.addPlayerReady_(event.playerInfo);
  console.log('readyplayers: ' + this.game_.players.length + ', all players: ' + this.gameManager_.getConnectedPlayers().length);
  if (this.game_.players.length == this.gameManager_.getConnectedPlayers().length) {
	  this.game_.retainPlayerOrder();
	  this.game_.updatePlayerTexts();
	  console.log("All players ready: ")
	  for (var p in this.game_.players) {
		  console.log("\t" + this.game_.players[p].playerId);
	  }
	  this.game_.DealCards();
	  this.game_.oldPlayers = this.game_.players.slice();
	  this.stateMachine_.goToState(
        sheepcast.games.spellcast.messages.GameStateId.PICK);
  }
};

sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.addPlayerReady_ =
    function(newPlayer) {
		for (var i = 0; i < this.game_.players.length; i++) {
			if (this.game_.players[i].playerId == newPlayer.playerId) {
				return;
			}
		}
		
		this.game_.players.push(newPlayer);
		
		if(this.game_.virgin)
			this.game_.updatePlayerTexts();
		
};

/**
 * Adds a player. Updates playerData with host flag.
 * @param {string} playerId
 * @param {Object} playerData
 * @param {boolean} isHost
 * @private
 */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.addPlayer_ =
    function(playerId, playerData, isHost) {
  if (isHost) {
    this.updateHost_(playerId, playerData);
  } else {
    playerData = playerData || {};
    playerData['host'] = false;
    this.gameManager_.updatePlayerData(playerId, playerData);
  }

  var parsedPlayerData =
      /** @type {!sheepcast.games.spellcast.messages.PlayerReadyData} */ (
          playerData);

  var player = this.game_.createPlayer(
      /* id */ playerId,
      /* name */ parsedPlayerData.playerName || '???',
      /* avatarindex */ parsedPlayerData.avatarIndex);

  player.activate(player.lobbyPosition, /* showNameText */ true);
};


/**
 * Sets a player to become the host and updates the player data.
 * @param {string} newHostPlayerId
 * @param {Object=} opt_newHostPlayerData
 * @private
 */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.updateHost_ =
    function(newHostPlayerId, opt_newHostPlayerData) {
  if (newHostPlayerId == this.hostPlayerId_) {
    return;
  }

  if (newHostPlayerId != this.hostPlayerId_ && this.hostPlayerId_ &&
      this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
    var oldHostPlayer = this.gameManager_.getPlayer(this.hostPlayerId_);
    var oldHostPlayerData = oldHostPlayer.playerData || {};
    oldHostPlayerData['host'] = false;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, oldHostPlayerData,
        /* opt_noBroadcastUpdate*/ false);
  }

  var newHostPlayerData = null;
  if (opt_newHostPlayerData) {
    newHostPlayerData = opt_newHostPlayerData;
  } else {
    var newHostPlayer = this.gameManager_.getPlayer(newHostPlayerId);
    newHostPlayerData = newHostPlayer.playerData || {};
  }
  newHostPlayerData['host'] = true;
  this.hostPlayerId_ = newHostPlayerId;
  this.gameManager_.updatePlayerData(this.hostPlayerId_, newHostPlayerData);
};


/** @override */
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onExit =
    function(nextStateId) {
  // Stop listening to player events in this state.
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_READY,
      this.boundPlayerReadyCallback_);

  // Update all ready players to playing state. Hide ready and playing players.
  var players = this.gameManager_.getPlayers();
  for (var i = 0; i < players.length; i++) {
    var playerState = players[i].playerState;
    if (playerState == cast.receiver.games.PlayerState.READY) {
      this.gameManager_.updatePlayerState(players[i].playerId,
          cast.receiver.games.PlayerState.PLAYING, null);
    }
  }

  // The lobby is now closed for new players.
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED,
      null);
  //this.game_.getLobbyDisplay().deactivate();
};
