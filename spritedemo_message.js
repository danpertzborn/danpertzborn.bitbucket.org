// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
/** @fileoverview JSON serializable custom messages used by spritedemo. */

goog.provide('sheepcast.games.spritedemo.SpritedemoMessage');
goog.provide('sheepcast.games.spritedemo.SpritedemoMessageType');
goog.provide('sheepcast.games.spellcast.messages.GameStateId');
goog.provide('sheepcast.games.spellcast.messages.GameData');


//var cast = cast || {};
/** @enum {number} Type of objects to create in the demo. */
sheepcast.games.spritedemo.SpritedemoMessageType = {
  UNKNOWN: 0,
  SPRITE: 1,
  CARD: 2,
  PICK_PASS: 3
};

/**
 * @enum {number} The different spellcast game state identifiers.
 * @export
 */
sheepcast.games.spellcast.messages.GameStateId = {
  UNKNOWN: 0,
  WAITING_FOR_PLAYERS: 1,
  PICK: 2,
  ROUND: 3
};


/**
 * JSON serializable message from a sender to create an object in the demo.
 * @struct
 * @constructor
 * @export
 */
sheepcast.games.spritedemo.SpritedemoMessage = function() {
  /**
   * @type {cast.games.spritedemo.SpritedemoMessageType}
   */
  this.type = sheepcast.games.spritedemo.SpritedemoMessageType.UNKNOWN;
};


/**
 * JSON serializable game data that persists while the game is running. All
 * responses will include properties are exported to preserve their names during
 * compilation.
 * @struct
 * @constructor
 * @export
 */
sheepcast.games.spellcast.messages.GameData = function() {
  /**
   * @type {cast.games.spellcast.messages.GameStateId}
   */
  this.gameStateId = sheepcast.games.spellcast.messages.GameStateId.UNKNOWN;
};