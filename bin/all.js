var sheepcast = {games:{}};
sheepcast.games.spritedemo = {};
sheepcast.games.spellcast = {};
sheepcast.games.spellcast.messages = {};
sheepcast.games.spritedemo.SpritedemoMessageType = {UNKNOWN:0, SPRITE:1, CARD:2, PICK_PASS:3};
sheepcast.games.spellcast.messages.GameStateId = {UNKNOWN:0, WAITING_FOR_PLAYERS:1, PICK:2, ROUND:3};
sheepcast.games.spritedemo.SpritedemoMessage = function() {
  this.type = sheepcast.games.spritedemo.SpritedemoMessageType.UNKNOWN;
};
sheepcast.games.spellcast.messages.GameData = function() {
  this.gameStateId = sheepcast.games.spellcast.messages.GameStateId.UNKNOWN;
};
sheepcast.games.common = {};
sheepcast.games.common.receiver = {};
sheepcast.games.common.receiver.Game = function() {
};
sheepcast.games.spellcast.State = function() {
};
sheepcast.games.spellcast.StateMachine = function(a) {
  this.game_ = a;
  this.states_ = Object.create(null);
  this.currentStateId_ = sheepcast.games.spellcast.messages.GameStateId.UNKNOWN;
  this.currentState_ = null;
};
sheepcast.games.spellcast.StateMachine.prototype.addState = function(a, b) {
  this.states_[a] = b;
};
sheepcast.games.spellcast.StateMachine.prototype.removeState = function(a) {
  delete this.states_[a];
};
sheepcast.games.spellcast.StateMachine.prototype.goToState = function(a) {
  if (this.currentState_) {
    this.currentState_.onExit(a);
  }
  var b = this.currentStateId_;
  this.currentStateId_ = a;
  this.currentState_ = this.states_[a];
  if (!this.currentState_) {
    throw Error("No state found for " + a);
  }
  this.currentState_.onEnter(b);
  this.game_.broadcastGameStatus(this.currentStateId_);
};
sheepcast.games.spellcast.StateMachine.prototype.broadcastState = function() {
  this.game_.broadcastGameStatus(this.currentStateId_);
};
sheepcast.games.spellcast.StateMachine.prototype.getState = function(a) {
  return this.states_[a];
};
sheepcast.games.spellcast.StateMachine.prototype.getCurrentState = function() {
  return this.currentState_;
};
sheepcast.games.spellcast.StateMachine.prototype.update = function() {
  if (this.currentState_) {
    this.currentState_.onUpdate();
  }
};
sheepcast.games.spellcast.states = {};
sheepcast.games.spellcast.states.PickState = function(a, b, c, d, g, e) {
  this.sprites_ = c;
  this.canvasWidth_ = d;
  this.canvasHeight_ = g;
  this.container_ = e;
  this.game_ = a;
  this.gameManager_ = this.game_.gameManager_;
  this.stateMachine_ = b;
  this.readyPlayers_ = [];
  this.hostPlayerId_ = null;
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);
  this.numberSpritesAdded_ = 0;
};
sheepcast.games.spellcast.states.PickState.prototype.onEnter = function(a) {
  this.game_.blind = [];
  this.game_.clearDrawnCards();
  this.readyPlayers_ = this.gameManager_.getPlayersInState(cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  for (a = 0;a < this.readyPlayers_.length;a++) {
    var b = this.readyPlayers_[a];
    this.addPlayer_(b.playerId, b.playerData, 0 == a);
  }
  this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_READY, this.boundPlayerReadyCallback_);
  this.gameManager_.addEventListener(cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED, this.boundGameMessageCallback_);
  this.gameManager_.updateGameplayState(cast.receiver.games.GameplayState.SHOWING_INFO_SCREEN, null);
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.OPEN, null);
  this.currUser = (this.game_.deal_player + 1) % this.game_.players.length;
  console.log("deal_player: " + this.game_.deal_player + "\tcurrUser: " + this.currUser);
  this.letPlayerPickPass(this.currUser);
};
sheepcast.games.spellcast.states.PickState.prototype.onUpdate = function() {
  var a = Date.now();
  this.readyPlayers_ = this.gameManager_.getPlayersInState(cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  var b = this.readyPlayers_.length, c = this.hostPlayerId_ ? this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  c && this.hostPlayerId_ && 0 < b && c.playerState != cast.receiver.games.PlayerState.READY && this.updateHost_(this.readyPlayers_[0].playerId);
  this.game_.randomAiEnabled && a > this.waitingForRandomAiDelay_ && (b < sheepcast.games.spellcast.GameConstants.MAX_PLAYERS ? this.game_.testCreatePlayer() : this.game_.testStartGame(), this.waitingForRandomAiDelay_ += 1E3);
};
sheepcast.games.spellcast.states.PickState.prototype.pickPlayer = function(a) {
  var b = {type:"blind", cards:this.game_.deck.splice(0, 2)};
  this.gameManager_.sendGameMessageToPlayer(this.game_.players[a].playerId, b);
  this.game_.pick_player = a;
  this.game_.pick_player == this.game_.partner_player && (this.game_.partner_player = -1, console.log("Pick became own partner"));
  this.game_.round_scores = [];
};
sheepcast.games.spellcast.states.PickState.prototype.letPlayerPickPass = function(a) {
  this.game_.headerText.text = this.game_.players[a].name + "'s turn!\nPick or Pass?";
  this.gameManager_.sendGameMessageToPlayer(this.game_.players[a].playerId, {type:"let_pick_pass"});
};
sheepcast.games.spellcast.states.PickState.prototype.onGameMessage_ = function(a) {
  if (a.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription);
  } else {
    a = a.requestExtraMessageData;
    var b = sheepcast.games.spritedemo.SpritedemoMessageType;
    console.log(a);
    if (a.type == b.PICK_PASS) {
      "PASS" == a.pick_pass ? (console.log("deal_player: " + this.game_.deal_player + "\t Passed by User: " + this.currUser), this.currUser = (this.currUser + 1) % this.game_.players.length, this.currUser == this.game_.deal_player ? (console.log("force pick currUser" + this.currUser), this.pickPlayer(this.currUser), this.game_.headerText.text = this.game_.players[this.currUser].name + " must pick.\nAll other players have passed.") : this.letPlayerPickPass(this.currUser)) : this.pickPlayer(this.currUser)
      ;
    } else {
      if (this.game_.blind.push(a.cardString), console.log("Blind length: " + this.game_.blind.length), this.game_.round_scores[this.game_.pick_player] += cardPoints(a.cardString), 2 == this.game_.blind.length) {
        this.game_.round_count = 1;
        this.game_.trick_count = [];
        for (var c in this.game_.players) {
          this.game_.round_scores[c] = 0, this.game_.trick_count[c] = 0;
        }
        this.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.ROUND);
      }
    }
  }
};
sheepcast.games.spellcast.states.PickState.prototype.updateHost_ = function(a, b) {
  if (a != this.hostPlayerId_) {
    if (a != this.hostPlayerId_ && this.hostPlayerId_ && this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
      var c = this.gameManager_.getPlayer(this.hostPlayerId_).playerData || {};
      c.host = !1;
      this.gameManager_.updatePlayerData(this.hostPlayerId_, c, !1);
    }
    c = null;
    c = b ? b : this.gameManager_.getPlayer(a).playerData || {};
    c.host = !0;
    this.hostPlayerId_ = a;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, c);
  }
};
sheepcast.games.spellcast.states.PickState.prototype.onExit = function(a) {
  this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_READY, this.boundPlayerReadyCallback_);
  this.gameManager_.removeEventListener(cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED, this.boundGameMessageCallback_);
  a = this.gameManager_.getPlayers();
  for (var b = 0;b < a.length;b++) {
    a[b].playerState == cast.receiver.games.PlayerState.READY && this.gameManager_.updatePlayerState(a[b].playerId, cast.receiver.games.PlayerState.PLAYING, null);
  }
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED, null);
};
sheepcast.games.spellcast.states.RoundState = function(a, b, c, d, g, e) {
  this.sprites_ = c;
  this.canvasWidth_ = d;
  this.canvasHeight_ = g;
  this.container_ = e;
  this.game_ = a;
  this.gameManager_ = this.game_.gameManager_;
  this.stateMachine_ = b;
  this.readyPlayers_ = [];
  this.hostPlayerId_ = null;
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);
};
sheepcast.games.spellcast.states.RoundState.prototype.onEnter = function(a) {
  this.gameManager_.addEventListener(cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED, this.boundGameMessageCallback_);
  this.gameManager_.updateGameplayState(cast.receiver.games.GameplayState.SHOWING_INFO_SCREEN, null);
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.OPEN, null);
  this.currTrick = [];
  this.currUser = this.game_.startTrickPlayer;
  console.log("Deal player: " + this.game_.deal_player + "\tStarting player: " + this.currUser + "\tPlayer length: " + this.game_.players.length);
  this.ledCard = "";
  this.RequestCard(this.currUser, this.ledCard);
};
sheepcast.games.spellcast.states.RoundState.prototype.RequestCard = function(a, b) {
  this.gameManager_.sendGameMessageToPlayer(this.game_.players[a].playerId, {type:"PlayCard", ledCard:b});
  console.log("Player " + a + " can now play a card with ledCard: " + b);
  this.game_.headerText.text = this.game_.players[a].name + "'s turn!\nPlay a card.";
};
sheepcast.games.spellcast.states.RoundState.prototype.onUpdate = function() {
  var a = Date.now();
  this.readyPlayers_ = this.gameManager_.getPlayersInState(cast.receiver.games.PlayerState.READY, this.readyPlayers_);
  var b = this.readyPlayers_.length, c = this.hostPlayerId_ ? this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  c && this.hostPlayerId_ && 0 < b && c.playerState != cast.receiver.games.PlayerState.READY && this.updateHost_(this.readyPlayers_[0].playerId);
  this.game_.randomAiEnabled && a > this.waitingForRandomAiDelay_ && (b < sheepcast.games.spellcast.GameConstants.MAX_PLAYERS ? this.game_.testCreatePlayer() : this.game_.testStartGame(), this.waitingForRandomAiDelay_ += 1E3);
};
sheepcast.games.spellcast.states.RoundState.prototype.onGameMessage_ = function(a) {
  if (a.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription);
  } else {
    a = a.requestExtraMessageData;
    var b = sheepcast.games.spritedemo.SpritedemoMessageType;
    console.log(a);
    if (a.type == b.CARD) {
      b = a.cardString;
      "" == this.ledCard && this.game_.clearDrawnCards();
      var c = "assets/";
      console.log("DisplayCard: playedUser " + this.currUser);
      c = c.concat(a.cardFilename);
      a = PIXI.Sprite.fromImage(c);
      a.anchor.x = .5;
      a.anchor.y = .5;
      a.scale.x = a.scale.y = 2 * sheepcast.games.spritedemo.SpritedemoGame.SCALE;
      switch(this.currUser) {
        case 0:
          a.position.x = 1 * this.canvasWidth_ / 6;
          a.position.y = .4 * this.canvasHeight_;
          break;
        case 1:
          a.position.x = 2 * this.canvasWidth_ / 6;
          a.position.y = .58 * this.canvasHeight_;
          break;
        case 2:
          a.position.x = 3 * this.canvasWidth_ / 6;
          a.position.y = .67 * this.canvasHeight_;
          break;
        case 3:
          a.position.x = 4 * this.canvasWidth_ / 6;
          a.position.y = .58 * this.canvasHeight_;
          break;
        case 4:
          a.position.x = 5 * this.canvasWidth_ / 6, a.position.y = .4 * this.canvasHeight_;
      }
      this.game_.container_.addChild(a);
      this.currUser = (this.currUser + 1) % this.game_.players.length;
      "" == this.ledCard && (this.ledCard = b, console.log("ledCard set: " + b));
      this.currTrick.push(b);
      console.log(this.currTrick);
      this.currTrick.length == this.game_.players.length ? (console.log("End of a round: " + this.game_.round_count), a = {type:"trick", cards:this.currTrick}, console.log("Broadcast msg: " + a), this.gameManager_.sendGameMessageToAllConnectedPlayers(a), a = this.getWinner(), this.game_.startTrickPlayer = a, this.game_.trick_count[a] += 1, b = this.calculatePoints(), this.game_.round_scores[a] += b, console.log("Winner: player " + a + "\tpoints: " + b), this.game_.round_count++, 6 >= this.game_.round_count ? 
      (this.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.ROUND), a = this.game_.players[a].name, this.game_.headerText.text = a + " takes the trick!\n" + a + ", play the lead card for the next trick.") : (console.log("TODO: SCORE STATE"), console.log("Round Scores: " + this.game_.round_scores), this.assignPoints(), this.game_.deal_player = (this.game_.deal_player + 1) % this.game_.players.length, this.game_.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS))) : 
      this.RequestCard(this.currUser, this.ledCard);
    }
  }
};
sheepcast.games.spellcast.states.RoundState.prototype.calculatePoints = function() {
  var a = 0, b;
  for (b in this.currTrick) {
    a += cardPoints(this.currTrick[b]);
  }
  return a;
};
sheepcast.games.spellcast.states.RoundState.prototype.assignPoints = function() {
  var a = this.game_.round_scores[this.game_.pick_player], b = this.game_.trick_count[this.game_.pick_player];
  -1 != this.game_.partner_player && (a += this.game_.round_scores[this.game_.partner_player], b += this.game_.trick_count[this.game_.partner_player]);
  this.game_.virgin = !1;
  var c, d, g = !0;
  6 == b ? (console.log("Pick Team wins took all tricks"), this.game_.headerText.text = "Picker Team wins!\nPerfect hand!", a = 12, b = 6, c = 3, d = -3) : 0 == b ? (console.log("Opponent Team wins took all tricks"), this.game_.headerText.text = "Opponent Team wins!\nPerfect hand!", a = -12, b = -6, c = -3, d = 3, g = !1) : 91 <= a ? (console.log("Pick Team wins greatly"), this.game_.headerText.text = "Picker Team wins!\nThe Opponents did not get Schneidered!", a = 8, b = 4, c = 2, d = -2) : 61 <= 
  a ? (console.log("Pick Team wins normally"), this.game_.headerText.text = "Picker Team wins!", a = 4, b = 2, c = 1, d = -1) : 31 <= a ? (console.log("Opponent Team wins normally"), this.game_.headerText.text = "Opponent Team wins!", g = !1, a = -4, b = -2, c = -1, d = 1) : (console.log("Opponent Team wins greatly"), this.game_.headerText.text = "Opponent Team wins!\nThe Picker did not get Schneidered!", a = -8, b = -4, c = -2, d = 2, g = !1);
  for (var e = 0;e < this.game_.players.length;e++) {
    var f = this.game_.players[e].playerId, k;
    k = e == this.game_.partner_player ? c : e == this.game_.pick_player ? -1 == this.game_.partner_player ? a : b : d;
    this.game_.playerScores_[f] += k;
    f = 0 < k ? new PIXI.Text("+" + k, {font:"24px Arial", align:"center", fill:"#00ff00"}) : new PIXI.Text(k, {font:"24px Arial", align:"center", fill:"#ff0000"});
    f.position.x = this.game_.canvasWidth_ * (e + 1) / 6;
    f.position.y = this.game_.playerTexts[e].position.y + .07 * this.game_.canvasHeight_;
    f.anchor.x = .5;
    f.anchor.y = .5;
    this.game_.container_.addChild(f);
  }
  this.game_.updatePlayerTexts();
  for (var h in this.game_.players) {
    a = {type:"end", won:g, pick:this.game_.pick_player == h || this.game_.partner_player == h ? !0 : !1, count:this.game_.trick_count[h]}, console.log("End message to player " + h + ": " + a), this.gameManager_.sendGameMessageToPlayer(this.game_.players[h].playerId, a);
  }
};
function cardPoints(a) {
  switch(a.split(" ")[0]) {
    case "Ace":
      return 11;
    case "King":
      return 4;
    case "Queen":
      return 3;
    case "Jack":
      return 2;
    case "10":
      return 10;
    default:
      return 0;
  }
}
function isTrump(a) {
  a = a.split(" ");
  if ("Diamonds" == a[2]) {
    return !0;
  }
  switch(a[0]) {
    case "Queen":
      return !0;
    case "Jack":
      return !0;
  }
  return !1;
}
sheepcast.games.spellcast.states.RoundState.prototype.getWinner = function() {
  var a = this.currTrick;
  console.log("getWinner is checking trick: " + a);
  var b = a[0].split(" "), c = 0, d = b[2], g = getValue(b[0]), e, f = a[2], k = b[2];
  isTrump(a[0]) && (k = d = "Trump");
  for (var h = 0;h < a.length;h++) {
    var l = a[h], b = l.split(" ");
    e = getValue(b[0]);
    f = b[2];
    console.log("winning card value:" + g);
    console.log("temp card value:" + e);
    isTrump(l) && (12 == e ? "Hearts" == f ? e += 1 : "Spades" == f ? e += 2 : "Clubs" == f && (e += 3) : 13 == e && (e += 10, "Hearts" == f ? e += 1 : "Spades" == f ? e += 2 : "Clubs" == f && (e += 3)), f = "Trump");
    "Trump" == f ? "Trump" == d ? g < e && (d = f, c = h, g = e) : (d = f, c = h, g = e) : "Trump" != d && f == k && (d == k ? e > g && (d = f, c = h, g = e) : (d = f, c = h, g = e));
  }
  console.log("winner Index: " + c + "\tStartTrickPlayer: " + this.game_.startTrickPlayer + "\tplayerLength: " + this.game_.players.length);
  return (this.game_.startTrickPlayer + c) % this.game_.players.length;
};
function getValue(a) {
  return "Ace" == a ? 11 : "Jack" == a ? 12 : "Queen" == a ? 13 : "King" == a ? 9 : "10" == a ? 10 : "9" == a ? 8 : "8" == a ? 7 : "7" == a ? 6 : 0;
}
sheepcast.games.spellcast.states.RoundState.prototype.updateHost_ = function(a, b) {
  if (a != this.hostPlayerId_) {
    if (a != this.hostPlayerId_ && this.hostPlayerId_ && this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
      var c = this.gameManager_.getPlayer(this.hostPlayerId_).playerData || {};
      c.host = !1;
      this.gameManager_.updatePlayerData(this.hostPlayerId_, c, !1);
    }
    c = null;
    c = b ? b : this.gameManager_.getPlayer(a).playerData || {};
    c.host = !0;
    this.hostPlayerId_ = a;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, c);
  }
};
sheepcast.games.spellcast.states.RoundState.prototype.onExit = function(a) {
  this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_READY, this.boundPlayerReadyCallback_);
  this.gameManager_.removeEventListener(cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED, this.boundGameMessageCallback_);
  a = this.gameManager_.getPlayers();
  for (var b = 0;b < a.length;b++) {
    a[b].playerState == cast.receiver.games.PlayerState.READY && this.gameManager_.updatePlayerState(a[b].playerId, cast.receiver.games.PlayerState.PLAYING, null);
  }
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED, null);
};
sheepcast.games.spellcast.states.WaitingForPlayersState = function(a, b) {
  this.game_ = a;
  this.gameManager_ = this.game_.gameManager_;
  this.stateMachine_ = b;
  this.game_.players = [];
  this.hostPlayerId_ = null;
  this.boundPlayerReadyCallback_ = this.onPlayerReady_.bind(this);
  this.playersAvailable = 0;
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onEnter = function(a) {
  this.game_.players = this.gameManager_.getPlayersInState(cast.receiver.games.PlayerState.READY, this.game_.players);
  for (a = 0;a < this.game_.players.length;a++) {
    this.addPlayer_(this.game_.players[a].playerId, this.game_.players[a].playerData, 0 == a);
  }
  this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_READY, this.boundPlayerReadyCallback_);
  this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_AVAILABLE, this.onPlayerAvailable_.bind(this));
  this.onPlayerAvailable_();
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onPlayerAvailable_ = function() {
  console.log("onPlayerAvailable_()");
  var a = this.gameManager_.getConnectedPlayers();
  for (i in a) {
    this.game_.virgin && (this.game_.playerTexts[i].text = "Player " + (parseInt(i) + 1) + " is connected."), "undefined" == typeof this.game_.playerScores_[a[i].playerId] && (console.log("i = " + i + ",    typeof i = " + typeof i + ",    playerId = " + a[i].playerId), this.game_.playerScores_[a[i].playerId] = 0);
  }
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onUpdate = function() {
  var a = Date.now();
  this.game_.players = this.gameManager_.getPlayersInState(cast.receiver.games.PlayerState.READY, this.game_.players);
  var b = this.game_.players.length, c = this.hostPlayerId_ ? this.gameManager_.getPlayer(this.hostPlayerId_) : null;
  c && this.hostPlayerId_ && 0 < b && c.playerState != cast.receiver.games.PlayerState.READY && this.updateHost_(this.game_.players[0].playerId);
  this.game_.randomAiEnabled && a > this.waitingForRandomAiDelay_ && (b < sheepcast.games.spellcast.GameConstants.MAX_PLAYERS ? this.game_.testCreatePlayer() : this.game_.testStartGame(), this.waitingForRandomAiDelay_ += 1E3);
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onPlayerReady_ = function(a) {
  a.playerInfo.name = a.requestExtraMessageData.name;
  console.log("waiting for players state, onPlayerReady called");
  if (a.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription);
  } else {
    if (console.log("**** PLAYER READY UP ****"), !(this.game_.players.length > this.game_.MAX_PLAYERS_) && (this.addPlayerReady_(a.playerInfo), console.log("readyplayers: " + this.game_.players.length + ", all players: " + this.gameManager_.getConnectedPlayers().length), this.game_.players.length == this.gameManager_.getConnectedPlayers().length)) {
      this.game_.retainPlayerOrder();
      this.game_.updatePlayerTexts();
      console.log("All players ready: ");
      for (var b in this.game_.players) {
        console.log("\t" + this.game_.players[b].playerId);
      }
      this.game_.DealCards();
      this.game_.oldPlayers = this.game_.players.slice();
      this.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.PICK);
    }
  }
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.addPlayerReady_ = function(a) {
  for (var b = 0;b < this.game_.players.length;b++) {
    if (this.game_.players[b].playerId == a.playerId) {
      return;
    }
  }
  this.game_.players.push(a);
  this.game_.virgin && this.game_.updatePlayerTexts();
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.addPlayer_ = function(a, b, c) {
  c ? this.updateHost_(a, b) : (b = b || {}, b.host = !1, this.gameManager_.updatePlayerData(a, b));
  a = this.game_.createPlayer(a, b.playerName || "???", b.avatarIndex);
  a.activate(a.lobbyPosition, !0);
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.updateHost_ = function(a, b) {
  if (a != this.hostPlayerId_) {
    if (a != this.hostPlayerId_ && this.hostPlayerId_ && this.gameManager_.isPlayerConnected(this.hostPlayerId_)) {
      var c = this.gameManager_.getPlayer(this.hostPlayerId_).playerData || {};
      c.host = !1;
      this.gameManager_.updatePlayerData(this.hostPlayerId_, c, !1);
    }
    c = null;
    c = b ? b : this.gameManager_.getPlayer(a).playerData || {};
    c.host = !0;
    this.hostPlayerId_ = a;
    this.gameManager_.updatePlayerData(this.hostPlayerId_, c);
  }
};
sheepcast.games.spellcast.states.WaitingForPlayersState.prototype.onExit = function(a) {
  this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_READY, this.boundPlayerReadyCallback_);
  a = this.gameManager_.getPlayers();
  for (var b = 0;b < a.length;b++) {
    a[b].playerState == cast.receiver.games.PlayerState.READY && this.gameManager_.updatePlayerState(a[b].playerId, cast.receiver.games.PlayerState.PLAYING, null);
  }
  this.gameManager_.updateLobbyState(cast.receiver.games.LobbyState.CLOSED, null);
};
sheepcast.games.spritedemo.SpritedemoGame = function(a) {
  this.gameManager_ = a;
  this.debugUi = new cast.receiver.games.debug.DebugUI(this.gameManager_);
  this.canvasWidth_ = window.innerWidth;
  this.canvasHeight_ = window.innerHeight;
  this.sprites_ = [];
  this.spriteVelocities_ = [];
  this.numberSpritesAdded_ = 0;
  this.headerText = this.background_ = null;
  this.playerMap_ = {};
  this.MAX_PLAYERS_ = 5;
  this.playerScores_ = {};
  this.virgin = !0;
  this.gameData_ = new sheepcast.games.spellcast.messages.GameData;
  this.boundUpdateFunction_ = this.update_.bind(this);
  this.isRunning_ = this.isLoaded_ = !1;
  this.container_ = new PIXI.Container;
  this.renderer_ = new PIXI.WebGLRenderer(this.canvasWidth_, this.canvasHeight_);
  this.loader_ = new PIXI.loaders.Loader;
  this.loader_.add("assets/icon.png");
  this.loader_.add("assets/background.jpg");
  this.loader_.add("assets/01s.png");
  this.loader_.add("assets/01h.png");
  this.loader_.add("assets/01d.png");
  this.loader_.add("assets/01c.png");
  this.loader_.add("assets/07s.png");
  this.loader_.add("assets/07h.png");
  this.loader_.add("assets/07d.png");
  this.loader_.add("assets/07c.png");
  this.loader_.add("assets/08s.png");
  this.loader_.add("assets/08h.png");
  this.loader_.add("assets/08d.png");
  this.loader_.add("assets/08c.png");
  this.loader_.add("assets/09s.png");
  this.loader_.add("assets/09h.png");
  this.loader_.add("assets/09d.png");
  this.loader_.add("assets/09c.png");
  this.loader_.add("assets/10s.png");
  this.loader_.add("assets/10h.png");
  this.loader_.add("assets/10d.png");
  this.loader_.add("assets/10c.png");
  this.loader_.add("assets/11s.png");
  this.loader_.add("assets/11h.png");
  this.loader_.add("assets/11d.png");
  this.loader_.add("assets/11c.png");
  this.loader_.add("assets/12s.png");
  this.loader_.add("assets/12h.png");
  this.loader_.add("assets/12d.png");
  this.loader_.add("assets/12c.png");
  this.loader_.add("assets/13s.png");
  this.loader_.add("assets/13h.png");
  this.loader_.add("assets/13d.png");
  this.loader_.add("assets/13c.png");
  this.loader_.once("complete", this.onAssetsLoaded_.bind(this));
  this.loadedCallback_ = null;
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);
  this.boundPlayerAvailableCallback_ = this.onPlayerAvailable_.bind(this);
  this.boundPlayerQuitCallback_ = this.onPlayerQuit_.bind(this);
  this.stateMachine_ = new sheepcast.games.spellcast.StateMachine(this);
  this.stateMachine_.addState(sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS, new sheepcast.games.spellcast.states.WaitingForPlayersState(this, this.stateMachine_));
  this.stateMachine_.addState(sheepcast.games.spellcast.messages.GameStateId.PICK, new sheepcast.games.spellcast.states.PickState(this, this.stateMachine_, this.sprites_, this.canvasWidth_, this.canvasHeight_, this.container_));
  this.stateMachine_.addState(sheepcast.games.spellcast.messages.GameStateId.ROUND, new sheepcast.games.spellcast.states.RoundState(this, this.stateMachine_, this.sprites_, this.canvasWidth_, this.canvasHeight_, this.container_));
  this.deal_player = 0;
};
sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES = 200;
sheepcast.games.spritedemo.SpritedemoGame.SCALE = 1;
sheepcast.games.spritedemo.SpritedemoGame.getRandomInt = function(a, b) {
  return Math.floor(Math.random() * (b - a + 1)) + a;
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.run = function(a) {
  this.isRunning_ ? a() : (this.loadedCallback_ = a, this.isLoaded_ ? this.start_() : this.loader_.load());
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.stop = function() {
  this.loadedCallback_ || !this.isRunning_ ? this.loadedCallback_ = null : (this.isRunning_ = !1, document.body.removeChild(this.renderer_.view), this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_AVAILABLE, this.boundPlayerAvailableCallback_), this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_QUIT, this.boundPlayerQuitCallback_), this.gameManager_.removeEventListener(cast.receiver.games.EventType.PLAYER_DROPPED, this.boundPlayerQuitCallback_));
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.start_ = function() {
  if (this.loadedCallback_) {
    document.body.appendChild(this.renderer_.view);
    this.isRunning_ = !0;
    this.gameManager_.updateGameplayState(cast.receiver.games.GameplayState.RUNNING, null);
    requestAnimationFrame(this.boundUpdateFunction_);
    this.loadedCallback_();
    this.loadedCallback_ = null;
    this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_AVAILABLE, this.boundPlayerAvailableCallback_);
    this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_QUIT, this.boundPlayerQuitCallback_);
    this.gameManager_.addEventListener(cast.receiver.games.EventType.PLAYER_DROPPED, this.boundPlayerQuitCallback_);
    this.playerMap = {};
    for (var a = this.gameManager_.getPlayers(), b = 0;b < a.length;b++) {
      this.addPlayer_(a[b].playerId);
    }
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.addPlayer_ = function(a) {
  if (!this.playerMap_[a]) {
    var b = 0, c;
    for (c = 1;5 >= c;c++) {
      var d = !0;
      for (key in this.playerMap_) {
        if (this.playerMap_[key] == c) {
          d = !1;
          break;
        }
      }
      if (d) {
        b = c;
        break;
      }
    }
    this.playerMap_[a] = b;
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.onAssetsLoaded_ = function() {
  this.background_ = PIXI.Sprite.fromImage("assets/background.jpg");
  this.background_.position.x = this.background_.position.y = 0;
  this.background_.width = this.canvasWidth_;
  this.background_.height = this.canvasHeight_;
  this.headerText = new PIXI.Text("Sheepshead", {font:"50px Arial", fill:"#ffffff", align:"center"});
  this.headerText.anchor.x = .5;
  this.headerText.anchor.y = .5;
  this.headerText.position.x = 1 * this.canvasWidth_ / 2;
  this.headerText.position.y = 1 * this.canvasHeight_ / 8;
  this.container_.addChild(this.background_);
  this.container_.addChild(this.headerText);
  this.playerTexts = [];
  for (var a = 0;5 > a;a++) {
    var b = new PIXI.Text("Player " + (a + 1) + ",\nPlease Connect.", {font:"24px Arial", fill:"#ffffff", align:"center"});
    b.anchor.x = .5;
    b.anchor.y = .5;
    b.position.y = 7 * this.canvasHeight_ / 8;
    b.position.x = this.canvasWidth_ * (a + 1) / 6;
    b.position.y = 0 == a || 4 == a ? 7 * this.canvasHeight_ / 11 : 2 == a ? 7 * this.canvasHeight_ / 8 : 7 * this.canvasHeight_ / 9;
    this.playerTexts.push(b);
    this.container_.addChild(b);
  }
  this.start_();
  this.stateMachine_.goToState(sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS);
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.updatePlayerTexts = function() {
  for (var a in this.players) {
    this.playerTexts[a].text = this.players[a].name + "\n" + this.playerScores_[this.players[a].playerId];
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.onPlayerAvailable_ = function(a) {
  a.statusCode != cast.receiver.games.StatusCode.SUCCESS ? (console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription)) : (a = a.playerInfo.playerId, this.gameManager_.updatePlayerState(a, cast.receiver.games.PlayerState.PLAYING, null), this.addPlayer_(a), this.stateMachine_.broadcastState(), this.playerScores_[a] = 0);
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.onPlayerQuit_ = function(a) {
  a.statusCode != cast.receiver.games.StatusCode.SUCCESS ? (console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription)) : (delete this.playerMap_[a.playerInfo.playerId], 0 == this.gameManager_.getConnectedPlayers().length && (console.log("No more players connected. Tearing down game."), cast.receiver.CastReceiverManager.getInstance().stop()));
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.retainPlayerOrder = function() {
  this.oldPlayers = this.oldPlayers || [];
  if (this.players.length != this.oldPlayers.length) {
    console.log("new players length != old players length");
  } else {
    for (var a in this.players) {
      var b = !1, c = this.players[a], d;
      for (d in this.oldPlayers) {
        c.playerId == this.oldPlayers[d].playerId && (b = !0);
      }
      if (0 == b) {
        console.log("new player not in old players n: " + c);
        return;
      }
    }
    console.log("old players is the same as new players");
    this.players = this.oldPlayers;
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.clearDrawnCards = function() {
  for (;7 < this.container_.children.length;) {
    var a = this.container_.getChildAt(7);
    this.container_.removeChild(a);
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.onGameMessage_ = function(a) {
  if (a.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log("Error: Event status code: " + a.statusCode), console.log("Reason for error: " + a.errorDescription);
  } else {
    var b = a.requestExtraMessageData;
    if (b.type == sheepcast.games.spritedemo.SpritedemoMessageType.SPRITE) {
      if (this.numberSpritesAdded_ < sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES) {
        var c = this.sprites_[this.numberSpritesAdded_];
        c.position.x = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(c.width / 2, this.canvasWidth_ - c.width / 2);
        c.position.y = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(c.height / 2, this.canvasHeight_ - c.height / 2);
        this.numberSpritesAdded_ += 1;
        this.container_.addChild(c);
        var d = a.playerInfo.playerId;
        console.log("playerId " + d);
      } else {
        console.log("Maximum number of sprites added. Not adding a new one");
      }
      if (this.numberSpritesAdded_ < sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES) {
        var g = "assets/", d = a.playerInfo.playerId;
        console.log("PlayerId " + d);
        console.log("playerIdMapped " + this.playerMap_[d]);
        console.log("playerIdFormatted " + leftPad(this.playerMap_[d] + 2, 2).concat("h.png"));
        g = g.concat(b.card).concat(".png");
        c = PIXI.Sprite.fromImage(g);
        d = a.playerInfo.playerId;
        console.log("playerId " + d);
        b = {result:"sprity", cardFile:g};
        this.gameManager_.sendGameMessageToPlayer(d, b);
        c.anchor.x = .5;
        c.anchor.y = .5;
        c.scale.x = c.scale.y = sheepcast.games.spritedemo.SpritedemoGame.SCALE;
        c.position.x = this.canvasWidth_ * this.playerMap_[d] / 7;
        c.position.y = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(c.height / 2, this.canvasHeight_ - c.height / 2);
        this.sprites_[this.numberSpritesAdded] = c;
        this.spriteVelocities_[this.numberSpritesAdded] = {x:0, y:0};
        this.numberSpritesAdded_ += 1;
        this.container_.addChild(c);
      } else {
        console.log("Maximum number of sprites added. Not adding a new one");
      }
    }
  }
};
function leftPad(a, b) {
  for (var c = a + "";c.length < b;) {
    c = "0" + c;
  }
  return c;
}
sheepcast.games.spritedemo.SpritedemoGame.prototype.GetPlayerNum_ = function(a) {
  for (p in this.players) {
    if (this.players[p].playerId == a) {
      return Number(p);
    }
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.update_ = function(a) {
  if (this.isRunning_) {
    requestAnimationFrame(this.boundUpdateFunction_);
    for (a = 0;a < this.numberSpritesAdded_;a++) {
      this.sprites_[a].rotation += .1;
      this.spriteVelocities_[a].x += sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(-2, 2);
      this.spriteVelocities_[a].y += sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(-2, 2);
      10 < Math.abs(this.spriteVelocities_[a].x) && (this.spriteVelocities_[a].x *= .8);
      10 < Math.abs(this.spriteVelocities_[a].y) && (this.spriteVelocities_[a].y *= .8);
      this.sprites_[a].position.x += this.spriteVelocities_[a].x;
      this.sprites_[a].position.y += this.spriteVelocities_[a].y;
      var b = this.sprites_[a].position.x, c = this.sprites_[a].position.y;
      0 >= b ? (this.spriteVelocities_[a].x *= -1, this.sprites_[a].position.x = 0) : b >= this.canvasWidth_ && (this.spriteVelocities_[a].x *= -1, this.sprites_[a].position.x = this.canvasWidth_);
      0 >= c ? (this.spriteVelocities_[a].y *= -1, this.sprites_[a].position.y = 0) : c >= this.canvasHeight_ && (this.spriteVelocities_[a].y *= -1, this.sprites_[a].position.y = this.canvasHeight_);
    }
    this.renderer_.render(this.container_);
  }
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.broadcastGameStatus = function(a) {
  this.gameData_.gameStateId = a;
  this.gameManager_.updateGameData(this.gameData_);
  this.gameManager_.broadcastGameManagerStatus(null);
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.DealCards = function() {
  this.deck = this.GenerateDeck();
  this.deck = this.shuffle(this.deck);
  this.partner_player = -1;
  for (var a in this.players) {
    var b = this.deck.splice(0, 6), c = {type:"cards", cards:b}, d;
    for (d in b) {
      if ("Jack of Diamonds" == b[d]) {
        this.partner_player = this.GetPlayerNum_(this.players[a].playerId);
        break;
      }
    }
    this.gameManager_.sendGameMessageToPlayer(this.players[a].playerId, c);
  }
  console.log("Partner player: " + this.partner_player);
  console.log(this.players[a]);
  this.startTrickPlayer = (this.deal_player + 1) % this.players.length;
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.shuffle = function(a) {
  for (var b = a.length, c, d;0 !== b;) {
    d = Math.floor(Math.random() * b), --b, c = a[b], a[b] = a[d], a[d] = c;
  }
  return a;
};
sheepcast.games.spritedemo.SpritedemoGame.prototype.GenerateDeck = function() {
  ranks = "7 8 9 10 Jack Queen King Ace".split(" ");
  suits = ["Hearts", "Diamonds", "Spades", "Clubs"];
  for (var a = [], b = 0;b < ranks.length;b++) {
    for (var c = 0;c < suits.length;c++) {
      a.push(ranks[b] + " of " + suits[c]);
    }
  }
  return a;
};

