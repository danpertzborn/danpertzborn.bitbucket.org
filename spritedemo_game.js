// Copyright 2015 Google Inc. All Rights Reserved.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
goog.provide('sheepcast.games.spritedemo.SpritedemoGame');


goog.require('sheepcast.games.common.receiver.Game');
goog.require('sheepcast.games.spritedemo.SpritedemoMessageType');
goog.require('sheepcast.games.spellcast.StateMachine');
goog.require('sheepcast.games.spellcast.states.WaitingForPlayersState');
goog.require('sheepcast.games.spellcast.states.PickState');
goog.require('sheepcast.games.spellcast.states.RoundState');



/**
 * Sprite Demo game.
 *
 * Adds a sprite from a pool of sprites when a sender sends a custom game
 * message. Automatically transitions AVAILABLE players to the PLAYING state.
 *
 * @param {!cast.receiver.games.GameManager} gameManager
 * @constructor
 * @implements {sheepcast.games.common.receiver.Game}
 * @export
 */
sheepcast.games.spritedemo.SpritedemoGame = function(gameManager) {
  /** @private {!cast.receiver.games.GameManager} */
  this.gameManager_ = gameManager;

  /**
   * Debug only. Call debugUi.open() or close() to show and hide an overlay
   * showing game manager and player information while testing and debugging.
   * @public {cast.receiver.games.debug.DebugUI}
   */
  this.debugUi = new cast.receiver.games.debug.DebugUI(this.gameManager_);

  /** @private {number} */
  this.canvasWidth_ = window.innerWidth;

  /** @private {number} */
  this.canvasHeight_ = window.innerHeight;

  /** @private {Array} */
  this.sprites_ = [];

  /** @private {Array} */
  this.spriteVelocities_ = [];

  /** @private {number} */
  this.numberSpritesAdded_ = 0;

  /** @private {PIXI.Sprite} */
  this.background_ = null;
  
  this.headerText = null;
  this.playerMap_ = {};
  this.MAX_PLAYERS_ = 5;
  this.playerScores_ = {};
  
  this.virgin = true;
  

  /**
   * Game data shared with players by the game manager.
   * @private {!cast.games.spellcast.messages.GameData}
   */
  this.gameData_ = new sheepcast.games.spellcast.messages.GameData();

  /** @private {function(number)} Pre-bound call to #update. */
  this.boundUpdateFunction_ = this.update_.bind(this);

  /** @private {boolean} */
  this.isLoaded_ = false;

  /** @private {boolean} */
  this.isRunning_ = false;

  /** @private {!PIXI.Container} */
  this.container_ = new PIXI.Container();

  /** @private {!PIXI.WebGLRenderer} */
  this.renderer_ = new PIXI.WebGLRenderer(this.canvasWidth_,
      this.canvasHeight_);

  /** @private {!PIXI.loaders.Loader} */
  this.loader_ = new PIXI.loaders.Loader();
  this.loader_.add('assets/icon.png');
  this.loader_.add('assets/background.jpg');
  
  this.loader_.add('assets/01s.png');
  this.loader_.add('assets/01h.png');
  this.loader_.add('assets/01d.png');
  this.loader_.add('assets/01c.png');
  
  this.loader_.add('assets/07s.png');
  this.loader_.add('assets/07h.png');
  this.loader_.add('assets/07d.png');
  this.loader_.add('assets/07c.png');
  
  this.loader_.add('assets/08s.png');
  this.loader_.add('assets/08h.png');
  this.loader_.add('assets/08d.png');
  this.loader_.add('assets/08c.png');
  
  this.loader_.add('assets/09s.png');
  this.loader_.add('assets/09h.png');
  this.loader_.add('assets/09d.png');
  this.loader_.add('assets/09c.png');
  
  this.loader_.add('assets/10s.png');
  this.loader_.add('assets/10h.png');
  this.loader_.add('assets/10d.png');
  this.loader_.add('assets/10c.png');
  
  this.loader_.add('assets/11s.png');
  this.loader_.add('assets/11h.png');
  this.loader_.add('assets/11d.png');
  this.loader_.add('assets/11c.png');
  
  this.loader_.add('assets/12s.png');
  this.loader_.add('assets/12h.png');
  this.loader_.add('assets/12d.png');
  this.loader_.add('assets/12c.png');
  
  this.loader_.add('assets/13s.png');
  this.loader_.add('assets/13h.png');
  this.loader_.add('assets/13d.png');
  this.loader_.add('assets/13c.png');
  
  this.loader_.once('complete', this.onAssetsLoaded_.bind(this));

  /** @private {?function()} Callback used with #run. */
  this.loadedCallback_ = null;

  /**
   * Pre-bound custom message callback.
   * @private {function(cast.receiver.games.Event)}
   */
  this.boundGameMessageCallback_ = this.onGameMessage_.bind(this);

  /**
   * Pre-bound player connect callback.
   * @private {function(cast.receiver.games.Event)}
   */
  this.boundPlayerAvailableCallback_ = this.onPlayerAvailable_.bind(this);

  /**
   * Pre-bound player quit callback.
   * @private {function(cast.receiver.games.Event)}
   */
  this.boundPlayerQuitCallback_ = this.onPlayerQuit_.bind(this);
  
  this.stateMachine_ = new sheepcast.games.spellcast.StateMachine(this);
  this.stateMachine_.addState(
      sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS,
      new sheepcast.games.spellcast.states.WaitingForPlayersState(this,
          this.stateMachine_));
  this.stateMachine_.addState(
      sheepcast.games.spellcast.messages.GameStateId.PICK,
      new sheepcast.games.spellcast.states.PickState(this,
          this.stateMachine_, this.sprites_, this.canvasWidth_, this.canvasHeight_, this.container_));
  this.stateMachine_.addState(
      sheepcast.games.spellcast.messages.GameStateId.ROUND,
      new sheepcast.games.spellcast.states.RoundState(this,
          this.stateMachine_, this.sprites_, this.canvasWidth_, this.canvasHeight_, this.container_));

  this.deal_player = 0;
};


/**
 * Max number of sprites allowed on screen.
 * {number}
 */
sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES = 200;


/**
 * Default scale of sprites.
 * {number}
 */
sheepcast.games.spritedemo.SpritedemoGame.SCALE = 1;


/**
 * @param {number} min
 * @param {number} max
 * @return {number} Returns a random integer between min and max.
 */
sheepcast.games.spritedemo.SpritedemoGame.getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};


/**
 * Runs the game. Game should load if not loaded yet.
 * @param {function()} loadedCallback This function will be called when the game
 *     finishes loading or is already loaded and about to actually run.
 * @export
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.run = function(loadedCallback) {
  // If the game is already running, return immediately.
  if (this.isRunning_) {
    loadedCallback();
    return;
  }

  // Start loading if game not loaded yet.
  this.loadedCallback_ = loadedCallback;
  if (!this.isLoaded_) {
    this.loader_.load();
    return;
  }

  // Start running.
  this.start_();
};


/**
 * Stops the game.
 * @export
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.stop = function() {
  if (this.loadedCallback_ || !this.isRunning_) {
    this.loadedCallback_ = null;
    return;
  }

  this.isRunning_ = false;
  document.body.removeChild(this.renderer_.view);

  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_AVAILABLE,
      this.boundPlayerAvailableCallback_);
  /*this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);*/
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_QUIT,
      this.boundPlayerQuitCallback_);
  this.gameManager_.removeEventListener(
      cast.receiver.games.EventType.PLAYER_DROPPED,
      this.boundPlayerQuitCallback_);
};


/**
 * Adds the renderer and run the game. Calls loaded callback passed to #run.
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.start_ = function() {
  // If callback is null, the game was stopped already.
  if (!this.loadedCallback_) {
    return;
  }

  document.body.appendChild(this.renderer_.view);
  this.isRunning_ = true;
  this.gameManager_.updateGameplayState(
      cast.receiver.games.GameplayState.RUNNING, null);
  requestAnimationFrame(this.boundUpdateFunction_);

  this.loadedCallback_();
  this.loadedCallback_ = null;

  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_AVAILABLE,
      this.boundPlayerAvailableCallback_);
  /*this.gameManager_.addEventListener(
      cast.receiver.games.EventType.GAME_MESSAGE_RECEIVED,
      this.boundGameMessageCallback_);*/
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_QUIT,
      this.boundPlayerQuitCallback_);
  this.gameManager_.addEventListener(
      cast.receiver.games.EventType.PLAYER_DROPPED,
      this.boundPlayerQuitCallback_);
	  
  this.playerMap = {};
  // Add any already connected players.
  var players = this.gameManager_.getPlayers();
  for (var i = 0; i < players.length; i++) {
    this.addPlayer_(players[i].playerId);
  }
};

/**
 * Adds a player to the game.
 * @param {string} playerId
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.addPlayer_ = function(playerId) {
  // Check if player is already on the screen.
  var playerSprite = this.playerMap_[playerId];
  if (playerSprite) {
    return;
  }

  var playerNum = 0;
  var possNum;
  for (possNum = 1; possNum <= 5; possNum++) {
	var okNum = true;
	for (key in this.playerMap_) {
	  if (this.playerMap_[key] == possNum) {
		  okNum = false;
		  break;
	  }
	}
	if (!okNum) {
	  continue;
	}
	playerNum = possNum;
	break;
  }

  // Associate playerID with player ID.
  this.playerMap_[playerId] = playerNum;
  
};

/**
 * Called when all assets are loaded.
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.onAssetsLoaded_ = function() {
  this.background_ = PIXI.Sprite.fromImage('assets/background.jpg');
  this.background_.position.x = this.background_.position.y = 0;
  this.background_.width = this.canvasWidth_;
  this.background_.height = this.canvasHeight_;
  
  this.headerText = new PIXI.Text('Sheepshead',{font : '50px Arial', fill : '#ffffff', align : 'center'});
	this.headerText.anchor.x = 0.5;
	this.headerText.anchor.y = 0.5;
	this.headerText.position.x = this.canvasWidth_ * 1 / 2;
	this.headerText.position.y = this.canvasHeight_ * 1 / 8;
	
  
	this.container_.addChild(this.background_);
	this.container_.addChild(this.headerText);
	
	this.playerTexts = new Array();
	

  
	//put player's names on screen.
	for(var i = 0; i < 5; i++){
		var pt = new PIXI.Text("Player " + (i + 1) + ",\nPlease Connect.", 
				{font : '24px Arial', fill : '#ffffff', align : 'center'});
		
		pt.anchor.x = 0.5;
		pt.anchor.y = 0.5;
		pt.position.y = this.canvasHeight_ * 7 / 8;
		pt.position.x = this.canvasWidth_ * (i+1) / 6;
		
		if(i == 0 || i == 4)
			pt.position.y = this.canvasHeight_ * 7/11;
		
		else if ( i == 2)
			pt.position.y = this.canvasHeight_ * 7/8;
		
		else
			pt.position.y = this.canvasHeight_ * 7/9;
		
		this.playerTexts.push(pt)
		this.container_.addChild(pt);
	}

  this.start_();
  this.stateMachine_.goToState(
      sheepcast.games.spellcast.messages.GameStateId.WAITING_FOR_PLAYERS);
};

/**
 * Updates player names and player scores displayed on screen.
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.updatePlayerTexts = function(){
	for(var i in this.players)
		this.playerTexts[i].text = this.players[i].name + '\n' + 
				this.playerScores_[this.players[i].playerId];
}


/**
 * Handles when a player becomes available to the game manager.
 * @param {cast.receiver.games.Event} event
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.onPlayerAvailable_ =
    function(event) {
  if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  var playerId = /** @type {string} */ (event.playerInfo.playerId);
  // Automatically transition available players to playing state.
  this.gameManager_.updatePlayerState(playerId,
      cast.receiver.games.PlayerState.PLAYING, null);
	  
  this.addPlayer_(playerId);
  this.stateMachine_.broadcastState();
  this.playerScores_[playerId] = 0;
  
};


/**
 * Handles when a player disconnects from the game manager.
 * @param {cast.receiver.games.Event} event
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.onPlayerQuit_ =
    function(event) {
  if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  
  delete this.playerMap_[event.playerInfo.playerId];
  
  // Tear down the game if there are no more players. Might want to show a nice
  // UI with a countdown instead of tearing down instantly.
  var connectedPlayers = this.gameManager_.getConnectedPlayers();
  if (connectedPlayers.length == 0) {
    console.log('No more players connected. Tearing down game.');
    cast.receiver.CastReceiverManager.getInstance().stop();
  }
};

sheepcast.games.spritedemo.SpritedemoGame.prototype.retainPlayerOrder = function() {
  this.oldPlayers = this.oldPlayers || [];
  if (this.players.length != this.oldPlayers.length) {
    console.log("new players length != old players length")
    return;
  }
  var indexes = [];
  for (var newP in this.players) {
    var inOldPlayers = false
    var newPlayer = this.players[newP];
    for (var oldP in this.oldPlayers) {
      var oldPlayer = this.oldPlayers[oldP];
      if (newPlayer.playerId == oldPlayer.playerId) {
        inOldPlayers = true;
      }
    }
    if (inOldPlayers == false) {
      console.log("new player not in old players n: " + newPlayer)
      return;
    }
  }
  console.log("old players is the same as new players")
  this.players = this.oldPlayers;
};

sheepcast.games.spritedemo.SpritedemoGame.prototype.clearDrawnCards = function() {
  while(this.container_.children.length > 7){
    var child = this.container_.getChildAt(7);
    this.container_.removeChild(child);
  }
};

/**
 * Callback for game message sent via game manager.
 * @param {cast.receiver.games.Event} event
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.onGameMessage_ =
    function(event) {
  if (event.statusCode != cast.receiver.games.StatusCode.SUCCESS) {
    console.log('Error: Event status code: ' + event.statusCode);
    console.log('Reason for error: ' + event.errorDescription);
    return;
  }
  var message = /** @type {!sheepcast.games.spritedemo.SpritedemoMessage} */ (
      event.requestExtraMessageData);
  var SpritedemoMessageType = sheepcast.games.spritedemo.SpritedemoMessageType;

  if (message.type == SpritedemoMessageType.SPRITE) {
    if (this.numberSpritesAdded_ <
        sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES) {
      var sprite = this.sprites_[this.numberSpritesAdded_];
      sprite.position.x = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(
          sprite.width / 2, this.canvasWidth_ - sprite.width / 2);
      sprite.position.y = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(
          sprite.height / 2, this.canvasHeight_ - sprite.height / 2);
      this.numberSpritesAdded_ = this.numberSpritesAdded_ + 1;
      this.container_.addChild(sprite);
	  
	  //var playerId = this.gameManager_.getPlayers()[0].playerId;
	  var playerId = /** @type {string} */ (event.playerInfo.playerId);
	  console.log("playerId " + playerId);
    }
	else {
      console.log('Maximum number of sprites added. Not adding a new one');
    }
  //}	
  
/////////////////////RECEIVE A CARD/////////////////////////////////
	//else if (message.type == SpritedemoMessageType.CARD){
    if (this.numberSpritesAdded_ <
        sheepcast.games.spritedemo.SpritedemoGame.MAX_NUM_SPRITES) {
				
			var cardFilepath = 'assets/';
			var playerId = /** @type {string} */ (event.playerInfo.playerId);
			console.log('PlayerId '+playerId);
			console.log('playerIdMapped '+this.playerMap_[playerId]);
			console.log('playerIdFormatted '+leftPad(this.playerMap_[playerId]+2, 2).concat("h.png"));
			var cardFilepath = cardFilepath.concat( message.card).concat(".png");
			var sprite = PIXI.Sprite.fromImage( cardFilepath );
			
	  var playerId = /** @type {string} */ (event.playerInfo.playerId);
	  console.log("playerId " + playerId);
	  var message = { result: 'sprity', cardFile: cardFilepath };
	  this.gameManager_.sendGameMessageToPlayer(playerId, message);
			
			sprite.anchor.x = 0.5;
			sprite.anchor.y = 0.5;
			sprite.scale.x = sprite.scale.y = sheepcast.games.spritedemo.SpritedemoGame.SCALE;
			
	
			var playerNum = this.playerMap_[playerId];
			sprite.position.x = this.canvasWidth_ * (playerNum) / 7;/*sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(
				  sprite.width / 2, this.canvasWidth_ - sprite.width / 2);*/
			sprite.position.y = sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(
				  sprite.height / 2, this.canvasHeight_ - sprite.height / 2);
		  
			this.sprites_[ this.numberSpritesAdded ] = sprite;
			this.spriteVelocities_[ this.numberSpritesAdded ] = { x: 0, y: 0 };
			this.numberSpritesAdded_ += 1;
			this.container_.addChild(sprite);
		} 
	    else {
			console.log('Maximum number of sprites added. Not adding a new one');
		}
	}
 };

 function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

sheepcast.games.spritedemo.SpritedemoGame.prototype.GetPlayerNum_ = function(playerId) {
	for (p in this.players) {
		if (this.players[p].playerId == playerId) {
			return Number(p);
		}
	}
	return undefined;
}

/**
 * Updates the game on each animation frame.
 * @param {number} timestamp
 * @private
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.update_ = function(timestamp) {
  if (!this.isRunning_) {
    return;
  }

  requestAnimationFrame(this.boundUpdateFunction_);

  for (var i = 0; i < this.numberSpritesAdded_; i++) {
    this.sprites_[i].rotation += 0.1;

    // Make it steer in random direction.
    this.spriteVelocities_[i].x +=
        sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(-2, 2);
    this.spriteVelocities_[i].y +=
        sheepcast.games.spritedemo.SpritedemoGame.getRandomInt(-2, 2);

    if (Math.abs(this.spriteVelocities_[i].x) > 10) {
      this.spriteVelocities_[i].x *= 0.8;
    }

    if (Math.abs(this.spriteVelocities_[i].y) > 10) {
      this.spriteVelocities_[i].y *= 0.8;
    }

    // Move the sprite according to it's velocity.
    this.sprites_[i].position.x += this.spriteVelocities_[i].x;
    this.sprites_[i].position.y += this.spriteVelocities_[i].y;

    // Make sure the sprites don't fly off the screen.
    var spriteX = this.sprites_[i].position.x;
    var spriteY = this.sprites_[i].position.y;

    if (spriteX <= 0) {
      this.spriteVelocities_[i].x *= -1;
      this.sprites_[i].position.x = 0;
    } else if (spriteX >= this.canvasWidth_) {
      this.spriteVelocities_[i].x *= -1;
      this.sprites_[i].position.x = this.canvasWidth_;
    }

    if (spriteY <= 0) {
      this.spriteVelocities_[i].y *= -1;
      this.sprites_[i].position.y = 0;
    } else if (spriteY >= this.canvasHeight_) {
      this.spriteVelocities_[i].y *= -1;
      this.sprites_[i].position.y = this.canvasHeight_;
    }

  }

  this.renderer_.render(this.container_);
};



/**
 * Updates the game data persisted by the running game and broadcasts the
 * current status of the game to all players. Called by the state machine when
 * the current game state changes (e.g. from waiting for players screen to
 * player action phase screen).
 * @param {cast.games.spellcast.messages.GameStateId} gameStateId
 */
sheepcast.games.spritedemo.SpritedemoGame.prototype.broadcastGameStatus =
    function(gameStateId) {
  this.gameData_.gameStateId = gameStateId;

  this.gameManager_.updateGameData(this.gameData_);
  this.gameManager_.broadcastGameManagerStatus(/* exceptSenderId */ null);
};

sheepcast.games.spritedemo.SpritedemoGame.prototype.DealCards = function() {
	this.deck = this.GenerateDeck();
	this.deck = this.shuffle(this.deck);
	this.partner_player = -1;
	for (var p in this.players) {
		var hand = this.deck.splice(0,6); // deal 6 cards to each player, removed from deck
		var msg = {type:"cards", cards:hand};

		for(var card in hand){
		  if(hand[card] == "Jack of Diamonds"){
		    this.partner_player = this.GetPlayerNum_(this.players[p].playerId);
            break;
		  }
		}
		this.gameManager_.sendGameMessageToPlayer(this.players[p].playerId, msg);
	}
    console.log("Partner player: " + this.partner_player);
    console.log(this.players[p]);

    //TODO: increment deal player
	this.startTrickPlayer = (this.deal_player + 1) % this.players.length; // start play to the left of the dealer
};

sheepcast.games.spritedemo.SpritedemoGame.prototype.shuffle = function(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

sheepcast.games.spritedemo.SpritedemoGame.prototype.GenerateDeck = function() {
	ranks = ["7", "8", "9", "10", "Jack", "Queen", "King", "Ace"];
	suits = ["Hearts", "Diamonds", "Spades", "Clubs"];
	
	var deck = [];
	for (var r = 0; r < ranks.length; r++) {
		for (var s = 0; s < suits.length; s++) {
			deck.push(ranks[r] + " of " + suits[s]);
		}
	}
	
	return deck;
}
