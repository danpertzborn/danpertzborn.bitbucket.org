java -jar compiler.jar ^
--closure_entry_point=sheepcast.games.spritedemo.SpritedemoGame ^
--formatting=PRETTY_PRINT ^
--compilation_level SIMPLE_OPTIMIZATIONS ^
--js spritedemo_message.js spritedemo_game.js common/game.js common/object_pool.js ^
common/phaser_sprite_pool.js state.js state_machine.js states/*.js ^
--js_output_file bin/all.js

PAUSE

